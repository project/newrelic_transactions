<?php

/**
 * @file
 * Primary module hooks for New Relic Transactions module.
 */

/**
 * Implements hook_help().
 */
function newrelic_transactions_help($route_name, $route_match) {
  switch ($route_name) {
    // Main module help for the newrelic_transactions module.
    case 'help.page.newrelic_transactions':
      $output = '';
      $output .= '<h2>' . t('About') . '</h2>';
      $output .= '<p>' . t('Drupal module for naming transactions in New Relic according to route, bundle, and role.') . '</p>';
      $output .= '<p>';
      $output .= '<h2>' . t('Why') . '</h2>';
      $output .= '<p>' . t('By default, the transactions tab of New Relic is not very helpful with diagnosing performance issues with Drupal sites.
                            Lacking information on the architecture of Drupal, New Relic catagorizes transactions according to what functions are called the most,
                            which often results in transactions that are grouped by low-level functions such as permission checking or menu loading,
                            rather than the specific action that the user is trying to complete.') . '</p>';
      $output .= '<p>';
      $output .= '<p>' . t('This module names transactions based on the routing information of the page request, giving clear indication of what action
                            the user was trying to take on the site. For routes that have to do with entities, the id placeholder is replaced with the type of entity
                            that is being accessed. The hightest-weighted role is appended to the transaction, so transactions can be grouped by whether users are anonymous,
                            authenticated, or have other roles.') . '</p>';
      $output .= '<p>';
      $output .= t('Visit the <a href=":project_link">Project page</a> on Drupal.org for more information.', [
        ':project_link' => 'https://www.drupal.org/project/newrelic_transactions',
      ]);
      $output .= '</p>';

      return $output;
  }
}

/**
 * Implements hook_requirements().
 */
function newrelic_transactions_requirements($phase) {
  $requirements = array();

  switch ($phase) {
    // Requirements for status report page.
    case 'runtime':
      $requirement = [
        'title' => t('New Relic Transactions'),
      ];

      // If newrelic extension is not found on server.
      if (!extension_loaded('newrelic')) {
        // Set error message.
        $requirement += [
          'description' => t('The New Relic extension was not found on your server.'),
          'severity' => REQUIREMENT_WARNING,
        ];
      }
      else {
        // If extension is found, set an OK status message.
        $requirement += [
          'description' => t('The New Relic extension was found on your server.'),
          'severity' => REQUIREMENT_OK,
        ];
      }

      // Add requirement to return array.
      $requirements['newrelic_transactions'] = $requirement;
      break;
  }

  return $requirements;
}
